const ajax = {
    state: {
        netBase: '', //测试环境
        url: {}
    },
    getters: {},
    actions: {
        /**
         * @desc ajax 请求
         * @param {isLoadding} 是否开启 loadding，默认true 
         * @param {isPop} 是否开启错误弹框，默认true 
         * @param {url} 请求的地址
         * @param {urls} String ，完整请求地址（方便调试使用）
         * @param {isReject} 是否有错误回调，默认 false 
         * @param {isCloseLoadding} 是否关闭 loadding加载，默认true
         * @param {data} 请求的参数
         */

        uniSend({
            commit,
            getters
        }, opts) {
            // 是否开启loadding，默认开启
            var isLoadding = opts.isLoadding === undefined ? true : opts.isLoadding

            //是否关闭loadding
            var isCloseLoadding = opts.isCloseLoadding === undefined ? true : opts.isCloseLoadding;
            // 是否开始错误弹框，默认开启
            var isPop = opts.isPop === undefined ? true : opts.isPop;
            if (isLoadding) {
                commit('showLoading')
            }
            return new Promise((resolve, reject) => {
                const postData = opts.data || {};
                if (getters.getItem('openId')) {
                    postData.openId = getters.getItem('openId');
                }
                postData.createTime = Date.now();
                // #ifdef MP-WEIXIN
                postData.provider = 'weixin'
                // #endif

                // #ifdef MP-QQ
                postData.provider = 'qq'
                // #endif
                //云函数请求封装
                uniCloud.callFunction({
                        name: opts.url,
                        data: postData
                    })
                    .then((res) => {
                        //请求成功
                        if (res.success && res.result.code) {
                            const data = res.result.data || res.result;
                            console.log(data)
                            resolve(data);
                            // 清除loadding
                            if (isCloseLoadding) {
                                commit('hideLoading')
                            }
                        } else {
                            // 是否弹框
                            if (isPop) {
                                //防止 hideLoading 和 toast 同时启动，导致 toast 不弹出
                                setTimeout(() => {
                                    commit('toast', res.result.msg || '未知错误');
                                }, 20)
                            }
                            if (opts.isReject) {
                                reject();
                            }
                            // 清除loadding
                            if (isCloseLoadding) {
                                commit('hideLoading')
                            }
                        }
                    }).catch((res) => {
                        if (opts.isReject) {
                            reject(res);
                        }
                        // 清除loadding
                        if (isCloseLoadding) {
                            commit('hideLoading')
                        }
                    })
            })
        }
    }
}
export default ajax

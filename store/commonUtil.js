import festivalObj from './festivalData'
const commonUtil = {
	state: {
		festivalData: festivalObj.festivalData || [],
		restData: festivalObj.restData || [],
		workData: festivalObj.workData || [],
		isAuth: false, //是否有菜单和纪念日权限
		//跳转的 url
		jumpUrl: {
			//ready
			ready: "/pages/ready/ready",
			//纪念日
			commemorationDay: '/pages/commemoration-day/commemoration-day',
			//纪念日新增
			commemorationDayAdd: '/pages/commemoration-day/commemoration-day-add/commemoration-day-add',
			//时间计算
			timeCalc: '/pages/time-calc/time-calc',
			//放假安排
			holidays: '/pages/holidays/holidays',
			//个人
			user: '/pages/user/user',
			//设置纪念日背景
			setDayPic: '/pages/user/setDayPic/setDayPic',
		}
	},
	getters: {
		getFestivalData: state => state.festivalData,
		getRestData: state => state.restData,
		getWorkData: state => state.workData,
		/**
		 * @description 获取权限
		 */
		getAuth: state => state.isAuth,
		/**
		 * @description 获取storage中存储的值
		 * @param {Object} key
		 */
		getItem: state => key => {
			try {
				return uni.getStorageSync(key) || ''
			} catch (e) {
				// error
				return ''
			}
		},
		/**
		 * @desc 获取拼接后的 url
		 */
		getHandleJumpUrl: state => res => {
			var urlStr = '';
			//获取对象的属性名
			var params = res.param ? Object.keys(res.param) : [];
			//拼接字符串
			for (let index = 0; index < params.length; index++) {
				if (typeof res.param[params[index]] === "string") {
					urlStr += params[index] + "=" + res.param[params[index]];
				} else {
					urlStr += params[index] + "=" + JSON.stringify(res.param[params[index]] || '');
				}
				//判断是否是最后一个对象
				if ((index + 1) !== params.length) {
					urlStr += '&&';
				}
			}
			var url = urlStr ? state.jumpUrl[res.name] + '?' + urlStr : state.jumpUrl[res.name];
			return url;
		},
		getDateTime: state => date => {
			var now = new Date(date),
				y = now.getFullYear(),
				m = now.getMonth() + 1,
				d = now.getDate();
			// now.toTimeString().substr(0, 8)
			return y + "/" + (m < 10 ? "0" + m : m) + "/" + (d < 10 ? "0" + d : d);
		}
	},
	mutations: {
		/**
		 * @desc 修改权限
		 */
		setAuth(state, flag) {
			state.isAuth = flag || false;
		},
		/**
		 * @description 使用storage做本地缓存
		 * @param {String} key 键
		 * @param {String} value 值
		 */
		saveItem(state, opts) {
			if (!opts.key) {
				return;
			}
			try {
				uni.setStorageSync(opts.key, opts.value);
			} catch (e) {
				// error
			}
		},
		/**
		 * @description 清除指定缓存
		 * @param {String} key 键
		 */
		clearItem(state, key) {
			if (!key) {
				//清空所有同步缓存
				try {
					uni.clearStorageSync();
				} catch (e) {
					// error
				}
			} else {
				//清空某个同步缓存
				try {
					uni.removeStorageSync(key);
				} catch (e) {
					// error
				}
			}
		},
		/*
		 * 保留当前页面，跳转到应用内的某个指定页面，可以使用 uni.navigateBack 返回到原来页面。
		 * @param {url} 跳转路径
		 * @param {param}传递的参数 Object
		 */
		navigateTo(state, res) {
			//兼容之前的写法
			if (typeof res === "string") {
				uni.navigateTo({
					url: res
				})
			} else {
				uni.navigateTo({
					url: this.getters.getHandleJumpUrl(res)
					//success() {}
				})
			}
		},
		/*
		 * 关闭当前页面，跳转到应用内的某个指定页面。
		 * @param {url} 跳转路径
		 * @param {param}传递的参数 Object
		 */
		redirectTo(state, res) {
			//兼容之前的写法
			if (typeof res === "string") {
				uni.redirectTo({
					url: res
				})
			} else {
				uni.redirectTo({
					url: this.getters.getHandleJumpUrl(res)
					//success() {}
				})
			}
		},
		/*
		 * 关闭当前所有页面，跳转到应用内的某个指定页面。
		 * @param {url} 跳转路径
		 * @param {param}传递的参数 Object
		 */
		reLaunch(state, res) {
			//兼容之前的写法
			if (typeof res === "string") {
				uni.reLaunch({
					url: res
				})
			} else {
				uni.reLaunch({
					url: this.getters.getHandleJumpUrl(res)
					//success() {}
				})
			}
		},
		/*
		 * 跳转到指定 tabBar 页面，并关闭其他所有非 tabBar 页面
		 * @param url 跳转路径
		 */
		switchTab(state, res) {
			//兼容之前的写法
			if (typeof res === "string") {
				uni.switchTab({
					url: res
				})
			} else {
				uni.switchTab({
					url: this.getters.getHandleJumpUrl(res)
					//success() {}
				})
			}
		},
		/*
		 * 关闭当前页面，返回上一级或多级页面。可通过 getCurrentPages 获取当前的页面栈信息，决定需要返回几层。
		 * @param delta 默认值 1,返回的页面数，如果 delta 大于现有打开的页面数，则返回到首页
		 */
		navigateBack(state, delta) {
			//获取页面栈
			var pages = getCurrentPages();

			//如果当前只有一个页面存在，则返回到 home页面

			if (pages.length === 1) {
				this.commit('reLaunch', {
					name: 'home'
				})
			} else {
				uni.navigateBack({
					delta: delta || 1
				})
			}
		},
		/*
		 * 设置导航栏文字及样式
		 * @param  标题
		 */
		setNavBar(state, title) {
			uni.setNavigationBarTitle({
				title: title
			})
		},
		/**
		 * @description 停止页面刷新
		 */
		stopPullDownRefresh(state) {
			uni.stopPullDownRefresh()
		}
	},
	actions: {
		chooseImage({
			commit,
			getters
		}, filePath) {
			return new Promise((resolve, reject) => {
				if (filePath) {
					// 上传到服务器
					uniCloud.uploadFile({
						filePath: filePath,
						// onUploadProgress: function(progressEvent) {
						// 	console.log(progressEvent)
						// 	var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
						// },
						success(e) {
							// _this.fileID = e.fileID
							resolve(e.fileID)
						},
						fail(e) {
							reject(e);
						},
						complete() {}
					})
				} else {
					uni.chooseImage({
						count: 1,
						success(res) {
							if (res.tempFilePaths.length > 0) {
								let path = res.tempFilePaths[0]
								resolve(path)
							}
						}
					})
				}

			})
		}
	}
}
export default commonUtil

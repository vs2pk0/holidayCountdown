const activeFullYear = new Date().getFullYear();

let festivalData = [{
		name: '元旦',
		startTime: '01月01日（周五）',
		startTimeStr: activeFullYear + '-01-01',
		endTimeStr: activeFullYear + '-01-01',
		endTime: '01月01日（周日）',
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: '',
		day: '3天',
		state: 0
	},
	{
		name: '春节',
		startTime: '02月11日（周四）',
		endTime: '02月17日（周三）',
		startTimeStr: activeFullYear + '-02-11',
		endTimeStr: activeFullYear + '-02-17',
		day: '7天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	},
	{
		name: '清明节',
		startTime: '04月03日（周六）',
		endTime: '04月05日（周一）',
		startTimeStr: activeFullYear + '-04-03',
		endTimeStr: activeFullYear + '-04-05',
		day: '3天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	},

	{
		name: '劳动节',
		startTime: '05月01日（周六）',
		endTime: '05月05日（周三）',
		startTimeStr: activeFullYear + '-05-01',
		endTimeStr: activeFullYear + '-05-05',
		day: '5天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	},
	{
		name: '端午节',
		startTime: '06月12日（周六）',
		endTime: '06月14日（周一）',
		startTimeStr: activeFullYear + '-06-12',
		endTimeStr: activeFullYear + '-06-14',
		day: '3天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	},
	{
		name: '中秋节',
		startTime: '09月19日（周日）',
		endTime: '09月21日（周二）',
		startTimeStr: activeFullYear + '-09-19',
		endTimeStr: activeFullYear + '-09-21',
		day: '3天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	},
	{
		name: '国庆节',
		// bottomName: '中秋节',
		startTime: '10月01日（周五）',
		endTime: '10月07日（周四）',
		startTimeStr: activeFullYear + '-10-01',
		endTimeStr: activeFullYear + '-10-07',
		day: '7天',
		state: 0,
		startTimeStamp: '',
		endTimeStamp: '',
		countDown: ''
	}
]

let restData = festivalData.map(item => {
	const num = parseInt(item.day);
	let minRestData = new Array(num).fill('').map((it, key) => {
		return getDateTime(new Date(item.startTimeStr).getTime() + key * 86400000);
	});
	return minRestData;
}).flat();

let workData = [
	activeFullYear + '-02-20',
	activeFullYear + '-04-25',
	activeFullYear + '-05-08',
	activeFullYear + '-09-18',
	activeFullYear + '-09-26',
	activeFullYear + '-10-09',
];

function getDateTime(date) {
	var now = new Date(date),
		y = now.getFullYear(),
		m = now.getMonth() + 1,
		d = now.getDate();
	// now.toTimeString().substr(0, 8)
	return y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d);
}

export default {
	festivalData,
	restData,
	workData
};

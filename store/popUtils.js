const popUtils = {
    state: {
        isAuthorize: false, //是否显示授权页面
        isShowModal: false, //是否显示弹窗
        loading: true //是否进行了 loadding
    },
    getters: {
        getLoading: (state) => state.loading
    },
    mutations: {
        /**
         * @desc 获取authorize 状态
         */
        openShowAuthorize(state) {
            state.isAuthorize = true;
        },
        /**
         * @desc 获取authorize 状态
         */
        closeShowAuthorize(state) {
            state.isAuthorize = false;
        },
        /**
         * @desc 获取modal 状态
         */
        openShowModal(state) {
            state.isShowModal = true;
        },
        /**
         * @desc 获取modal 状态
         */
        closeShowModal(state) {
            state.isShowModal = false;
        },
        /*
         * 显示带图标的toast
         * @param title
         * @param icon toast 类型，展示相应图标，默认 none，支持 success / fail / exception / none’。其中 exception 类型必须传文字信息
         * @param duration 显示时长，单位为 ms，默认 1000
         * @param mask 透明层，防止触摸穿透，默认true
         */
        toast(state, opts) {
            if (state.loading) {
                uni.hideLoading();
            }
            setTimeout(() => {
                //如果传入的是个字符串
                if (typeof opts === "string") {
                    uni.showToast({
                        icon: 'none',
                        title: opts || '',
                        // #ifdef MP-WEIXIN
                        duration: 3000,
                        // #endif
                        //mask: true
                    });
                } else {
                    opts = opts || {};

                    uni.showToast({
                        icon: opts.type || 'none',
                        title: opts.msg || '',
                        // #ifdef MP-WEIXIN
                        duration: opts.duration || 3000,
                        // #endif
                        mask: true
                    });
                }
            }, 200)
        },
        /**
         * @description 隐藏toast
         */
        hideToast() {
            uni.hideToast();
        },
        /*
         * 显示loading
         * @param title loading文字
         * @param mask 透明层，防止触摸穿透，默认true
         * @update date:2019.05.20 by wanghui@ykkcn.com
         * @update content:loading改成原生的方式，解决微信小程序充值页面充值列表不兼容的问题
         */
        showLoading(state) {
            state.loading = true;
            uni.showLoading({
                title: (!state.title ? '加载中' : state.title),
                //#ifdef MP-WEIXIN
                mask: true
                // #endif
            })
        },
        /**
         * @description 隐藏加载提示
         */
        hideLoading(state) {
            // uni.hideLoading();
            state.loading = false;
            uni.hideLoading();
        },
        /**
         * @description 显示导航栏loading
         */
        showTabLoading() {
            uni.showNavigationBarLoading();
        },
        /**
         * @description 隐藏导航栏loading
         */
        hideTabLoading() {
            uni.hideNavigationBarLoading();
        }
    }
}
export default popUtils

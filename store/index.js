import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import popUtils from './popUtils'
import commonUtil from './commonUtil'
import verification from './verification'
import uniAjax from './uniAjax'
const store = new Vuex.Store({
    modules: {
        commonUtil,
        popUtils,
        verification,
        uniAjax
    }
})
export default store

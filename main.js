import Vue from 'vue'
import App from './App'
//引用vuex
import store from './store'
Vue.config.productionTip = false
Vue.prototype.$store = store
App.mpType = 'app'
//引入底部菜单
import footMenu from '@/components/foot-menu/foot-menu.vue'
//引入授权页面
import authorize from '@/components/authorize/authorize.vue'
Vue.component('foot-menu', footMenu);
const app = new Vue({
    store,
    ...App
})
app.$mount()

'use strict';
//获取用户数据
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const provider = event.provider === 'qq' ? '-qq' : '';
    const collection = db.collection('user-list' + provider);
    const myOpenID = event.openId;
    //event为客户端上传的参数
    let res = '';
    const checkCount = await collection.where({
        openId: dbCmd.eq(myOpenID)
    }).count()
    //有记录则返回
    if (checkCount.total) {
        res = await collection.where({
            openId: dbCmd.eq(myOpenID)
        }).get();
    } else {
        res = event;
    }
    //返回数据给客户端
    return {
        code: 1,
        data: res.data ? res.data[0] : {},
        msg: '请求成功'
    }
};

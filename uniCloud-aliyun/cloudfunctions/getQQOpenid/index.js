'use strict';
//根据code获取openid
exports.main = async (event) => {
    //event为客户端上传的参数
    //qq小程序，码上放假
    const appId = '1109772580';
    const secret = 'lR9bWkVojDoH9YQJ';
    const apiUrl = 'https://api.q.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + event.code + '&grant_type=authorization_code';
    const res = await uniCloud.httpclient.request(apiUrl, {
        method: 'POST',
        dataType: 'json'
    })
    const data = {
        code: 1,
		data: {
		    ...res.data,
		    isAuth: false
		},
        msg: '请求成功'
    }
    //返回数据给客户端
    return data
};

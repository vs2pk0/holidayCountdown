"use strict";
//qq发送通知
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event) => {
    const collection = db.collection("memorial-day-qq");

    const nowTime = new Date().getTime();
    const initData = await collection.where({
        startDateStr: dbCmd.lt(nowTime),
        // endDateStr: dbCmd.gt(nowTime),
        status: dbCmd.neq(1)
    }).get();

    if (!initData.data.length) {
        return {
            code: 0,
            data: "没有发送的通知",
            msg: "请求成功"
        }
    }
    //event为客户端上传的参数
    const appId = "1109772580";
    const secret = "lR9bWkVojDoH9YQJ";
    const apiUrl = "https://api.q.qq.com/api/getToken?grant_type=client_credential&appid=" + appId + "&secret=" + secret;
    const res = await uniCloud.httpclient.request(apiUrl, {
        method: "POST",
        dataType: "json"
    })
    const access_token = res.data.access_token
    //获取字典

    const dic = await uniCloud.callFunction({
        name: "getDic",
    })
    const arrayCategory = dic.result.data.category;
    const arrayTip = dic.result.data.tip;
    //发送订阅消息
    // const arrayCategory = ["倒计时", "结婚纪念日", "恋爱", "出生", "生日", "工作", "其他"];
    // const arrayTip = ["一次提醒", "每年", "每周", "每天"];
    const url = "https://api.q.qq.com/api/json/subscribe/SendSubscriptionMessage?access_token=" + access_token
    async function dbFuc () {
        const arrs = []
        for (let q of initData.data) {
            const message = await uniCloud.httpclient.request(url, {
                method: "POST",
                dataType: "json",
                headers: {
                    "content-type": "application/json"
                },
                data: {
                    touser: q.openId, //		是	接收者（用户）的 openid
                    template_id: "f74ea7730d1fba2b6d744e92f71d9119", //		string		是	所需下发的订阅消息的id
                    "page": "/pages/ready/ready",
                    "data": {
                        "keyword1": {
                            "value": q.name
                        },
                        "keyword2": {
                            "value": q.date
                        },
                        "keyword3": {
                            "value": arrayCategory[q.category]
                        }
                    },
                    // "emphasis_keyword": "keyword1.DATA"
                },
                success: function (res) {
                    console.log(res)
                },
                fail: function (err) {
                    console.log("request fail ", err);
                }
            })
            //修改一次性倒计时状态
            if (q.tip == 0) {
                await collection.where({
                    _id: dbCmd.eq(q._id)
                }).update({
                    status: 1
                });
            }
            // arrs.push(1);
            arrs.push(message.data);
        }
        return await {
            code: 1,
            data: arrs,
            msg: "请求成功"
        };
    }

    //返回数据给客户端
    return dbFuc();
};

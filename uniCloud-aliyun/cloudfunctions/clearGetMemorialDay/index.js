'use strict';
//获取用户数据
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const provider = event.provider === 'qq' ? '-qq' : '';
    const collection = db.collection('memorial-day' + provider);
    const res = await collection.doc(event._id).remove()
    //返回数据给客户端
    return {
        code: 1,
        data: res.data,
        msg: '请求成功'
    }
};

'use strict';
//根据code获取openid
exports.main = async (event) => {
    //event为客户端上传的参数
    //2020假期倒计时
    const appId = 'wxa9efdbbc0088d57e';
    const secret = 'f5c54647ae3099e5ee57d47363ed3b85';
    const apiUrl = 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + event.code + '&grant_type=authorization_code';
    const res = await uniCloud.httpclient.request(apiUrl, {
        method: 'POST',
        dataType: 'json'
    })
    const data = {
        code: 1,
        data: {
            ...res.data,
            isAuth: false
        },
        msg: '请求成功'
    }
    //返回数据给客户端
    return data
};

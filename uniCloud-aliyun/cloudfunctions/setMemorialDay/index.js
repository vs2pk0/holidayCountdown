'use strict';
//设置纪念日
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const provider = event.provider === 'qq' ? '-qq' : '';
    const collection = db.collection('memorial-day' + provider);
    const myOpenID = event.openId;
    //event为客户端上传的参数
    let res = '';
    const checkCount = await collection.where({
        openId: dbCmd.eq(myOpenID)
    }).count()
    const createTime = Date.now();
    // const count = res.count();
    //有记录则更新，否则插入数据
    if (checkCount.total >= 10) {
        res = {
            code: 0,
            data: {},
            msg: '纪念日不能最多设置10条'
        };
    } else {
        let data = await collection.add({
            createTime,
            ...event
        });
        res = {
            code: 1,
            data: data.data,
            msg: '保存成功'
        };

    }
    //返回数据给客户端
    return res
};

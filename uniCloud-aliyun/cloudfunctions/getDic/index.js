'use strict';
//获取用户数据
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const collection = db.collection('dictionaries');
    //event为客户端上传的参数
    const res = await collection.field({ '_id': false }).get()
    //返回数据给客户端
    return {
        code: 1,
        data: res.data ? res.data[0] : {},
        msg: '请求成功'
    }
};

'use strict';
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const provider = event.provider === 'qq' ? '-qq' : '';
    const collection = db.collection('user-list' + provider);
    const myOpenID = event.openId;
    //event为客户端上传的参数
    let res = '';
    const checkCount = await collection.where({
        openId: dbCmd.eq(myOpenID)
    }).count()
    const createTime = Date.now();
    //有记录则更新，否则插入数据
    if (checkCount.total) {
        res = await collection.where({
            openId: dbCmd.eq(myOpenID)
        }).update(event);
    } else {
        res = await collection.add({
            createTime,
            ...event
        });
    }
    //返回数据给客户端
    return {
        code: 1,
        data: event,
        msg: '请求成功'
    }
};

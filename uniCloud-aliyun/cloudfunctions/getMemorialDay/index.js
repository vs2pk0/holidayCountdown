'use strict';
//获取纪念日列表
const db = uniCloud.database()
const dbCmd = db.command;
exports.main = async (event, context) => {
    const provider = event.provider === 'qq' ? '-qq' : '';
    const collection = db.collection('memorial-day' + provider);
    const myOpenID = event.openId;
    let res = null;
    //查询数据
    res = await collection.where({
        openId: dbCmd.eq(myOpenID)
        // }).orderBy("topping", "desc").orderBy("endDateStr", "desc").get();
    }).orderBy("endDateStr", "desc").orderBy("topping", "desc").get();
    //返回数据给客户端
    return {
        code: 1,
        data: res.data || [],
        msg: '请求成功'
    }
};

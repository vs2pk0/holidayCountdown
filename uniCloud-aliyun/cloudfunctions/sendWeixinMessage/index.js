'use strict';
'use strict';
//根据code获取openid
exports.main = async (event) => {
    //event为客户端上传的参数
    //2020假期倒计时
    const appId = 'wxa9efdbbc0088d57e';
    const secret = 'f5c54647ae3099e5ee57d47363ed3b85';
    const apiUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appId + '&secret=' + secret;
    const res = await uniCloud.httpclient.request(apiUrl, {
        method: 'POST',
        dataType: 'json'
    })
    const access_token = res.data.access_token
    console.log("access_token", access_token)
    const url = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=' + access_token
    const message = await uniCloud.httpclient.request(url, {
        method: 'POST',
        // dataType: 'json',
        headers: {
            "content-type": 'application/json'
        },
        data: {
			// "form_id": "FORMID",
            "touser": "o2GI347WeXbsDlnQ4y5ATopGv92I", //		是	接收者（用户）的 openid
            "template_id": "NDnLhBFeOJKdZwd-H4NnCfFpaXd_tJo4IuCfCB-_K3o",
            "page": "pages/ready/ready",
            "data": {
                "keyword1": {
                    "value": "339208499"
                },
                "keyword2": {
                    "value": "2015年01月05日 12:30"
                },
                "keyword3": {
                    "value": "腾讯微信总部"
                }
            },
            "emphasis_keyword": "keyword1.DATA", //要放大的数据
        },
        success: function (res) {
            console.log(res)
        },
        fail: function (err) {
            console.log('request fail ', err);
        }
    })
    const data = {
        code: 1,
        data: message.data,
        msg: '请求成功'
    }
    //返回数据给客户端
    return data
};


// https: //api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=ACCESS_TOKEN
